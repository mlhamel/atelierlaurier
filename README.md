atelierlaurier
==============

Hosted at http://atelierlaurier.ca

How
---

```
$ virtualenv atelierlaurier
$ atelierlaurier/bin/activate
$ pip install requirements.txt
$ pserve development.ini
```
